import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { User } from '../user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  username : string | undefined
  loggedInUser : User | undefined

  constructor(public loginService : LoginService) { }

  ngOnInit(): void {
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');
    this.username  = this.loggedInUser?.firstName+" "+this.loggedInUser?.lastName;
  }

}

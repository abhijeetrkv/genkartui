import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { Report } from 'src/app/report/report.component';
import { User } from 'src/app/user';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http:HttpClient) { }
  loggedInUser: User | undefined

  getFilteredReports(fromDate: any, toDate: any) {
    //Fetch logged in user ID
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');

    return this.http.get<Report[]>(`${API_URL}/filteredReports/${this.loggedInUser?.id}/${fromDate}/${toDate}`);
  }
}

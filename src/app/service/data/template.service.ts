import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/app.constants';
import { Template } from 'src/app/generate/generate.component';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(private http:HttpClient) { }


  getUserAssignedTemplates(userId: any) {
    return this.http.get<Template[]>(`${API_URL}/templates/${userId}`);
  }
}

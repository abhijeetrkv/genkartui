import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../app.constants';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private http: HttpClient) { }
  loggedInUser: User | undefined

  // Returns an observable 
  generateCertificate(templateId: String,file: any, zipfile:any, logoImage:any, signImage:any): Observable<any> {

    // Create form data 
    const formData = new FormData();

    // Store form name as "file" with file data 
    formData.append("file", file, file.name);
    
    if(zipfile  === undefined){
      formData.append("zipfile", file, "null");
    }else{
      formData.append("zipfile", zipfile, zipfile.name);
    }

    if(logoImage  === undefined){
      formData.append("logoImage", file, "null");
    }else{
      formData.append("logoImage", logoImage, logoImage.name);
    }

    if(signImage  === undefined){
      formData.append("signImage", file, "null");
    }else{
      formData.append("signImage", signImage, signImage.name);
    }
    
    //Fetch logged in user ID
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');

    // Make http post request over api 
    // with formData as req 
    return this.http.post(`${API_URL}/generateCertificate/${templateId}/${this.loggedInUser?.id}`, formData)
  }

  downloadFile(): any {
    return this.http.get(API_URL + "/downloadCertificateZip", { responseType: 'blob' });
  }

  getProgressBarPercentage(): any {
    return this.http.get(API_URL + "/loader/getProgressBarPercentage");
  }
}
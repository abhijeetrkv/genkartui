import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../user';
import { API_URL } from './../app.constants';

export const TOKEN = 'token'
export const AUTHENTICATED_USER = 'authenticaterUser'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public loginfromSpringBoot(user : User): Observable<any>{
    return this.http.post<any>(API_URL+"/users/login",user)
  }

  executeAuthenticationService(username: string, password: string) {
    
    let basicAuthHeaderString = window.btoa(username + ':' + password);

    let headers = new HttpHeaders({
        Authorization: basicAuthHeaderString
      })

    return this.http.get<User>(
      `${API_URL}/basicauth`,
      {headers}).pipe(
        map(
          data => {
            sessionStorage.setItem(AUTHENTICATED_USER, username);
            sessionStorage.setItem(TOKEN, basicAuthHeaderString);
            return data;
          }
        )
      );
    //console.log("Execute Hello World Bean Service")
  }

  // public getLoggedInUser() : Observable<User>{
  //   return this.http.get<User>(API_URL+"/getLoggedInUser") 
  // }

  public registerfromSpringBoot(user : User): Observable<any>{
    return this.http.post<any>(API_URL+"/users/register",user)
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('authenticaterUser')
    return !(user === null)
  }

  logout(){
    sessionStorage.removeItem('authenticaterUser')
    sessionStorage.setItem('loggedInUser','')
    localStorage.removeItem("IsLoadedBefore");
  }
  
}

export class AuthenticationBean{
  constructor(public message:string) { }
}
import { LoginService } from './../service/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = ''
  password = ''
  errorMessage = 'Invalid Credentials'
  invalidLogin = false
  
  constructor(private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
  }

  //**********************Depreceated*****************************
  handleLogin() {
    let user = new User(0, this.username, '', this.password,
      '', '', '', new Date);
    this.loginService.loginfromSpringBoot(user).subscribe(
      data => {
        sessionStorage.setItem('authenticaterUser', this.username);
        this.router.navigate(['dashboard'])
        this.invalidLogin = false
      },
      error => {
        console.log(error)
        this.invalidLogin = true
      }

    )
  }

  handleBasicAuthLogin() {
    this.loginService.executeAuthenticationService(this.username, this.password)
        .subscribe(
          data => {
            console.log(data)
            let loggeduser = new User(data.id,data.userName,data.email,data.password,data.firstName
              ,data.lastName,data.createdBy,data.createdOn);
            localStorage.setItem('loggedInUser', JSON.stringify(loggeduser));

            sessionStorage.setItem('authenticaterUser', this.username);
            this.router.navigate(['dashboard'])
            this.invalidLogin = false      
          },
          error => {
            console.log(error)
            this.invalidLogin = true
            alert("Invalid Username or Password, Please try again.")
          }
        )
  }
}
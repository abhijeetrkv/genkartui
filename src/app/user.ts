export class User {
    id: number;
    userName: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    createdBy: string
    createdOn: Date

    constructor(id: number,
        userName: string,
        email: string,
        password: string,
        firstName: string,
        lastName: string,
        createdBy: string,
        createdOn: Date){
            this.id = id;
            this.email = email;
            this.userName = userName;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.createdBy = createdBy;
            this.createdOn = createdOn;
        }

}

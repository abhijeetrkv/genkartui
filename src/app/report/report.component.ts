import { Component, OnInit } from '@angular/core';
import { DaySeries } from '@fullcalendar/core';
import { ReportService } from '../service/data/report.service';
import { LoaderService } from '../service/loader/loader.service';
import * as XLSX from 'xlsx'; 


export class Report {
  constructor(
    public id: number,
    public generatedBy: string,
    public sid: string,
    public generatedOn: Date,
    public candidateName: string,
    public courseName: string,
    public level: string,
    public templateName: string,
    public trainingPartner: string,
    public batchId: string,
  ) {

  }
}

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  fromDate=''
  toDate=''
  reports: Report[] | undefined

  constructor(private reportService : ReportService,
    public loaderService : LoaderService) { }

  ngOnInit(): void {
  }

  generateReport() {
    //console.log(this.fromDate)
    //console.log(this.toDate)
    let datedifference = Math.floor((new Date(this.toDate).getTime() - new Date(this.fromDate).getTime()) / 1000 / 60 / 60 / 24);
    
    if(datedifference>0 && datedifference<=90){
      this.reportService.getFilteredReports(this.fromDate,this.toDate).subscribe(
        response => {
          //console.log(response);
          this.reports = response;
        }
      )
    }else{
      alert("Please select date range within 90 days")
    }
    

  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-table'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, 'ReportData.xlsx');
			
    }
}

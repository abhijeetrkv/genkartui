import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ReportComponent } from './report/report.component';
import { GenerateComponent } from './generate/generate.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FpasswordComponent } from './fpassword/fpassword.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { LogoutComponent } from './logout/logout.component';
import { ErrorComponent } from './error/error.component';
import { FormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { HttpInterceptorService } from './service/http/http-interceptor.service';
import { HashLocationStrategy,LocationStrategy } from '@angular/common';
import { NgCircleProgressModule } from 'ng-circle-progress';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfileComponent,
    GenerateComponent,
    ReportComponent,
    GenerateComponent,
    LoginComponent,
    RegisterComponent,
    FpasswordComponent,
    FooterComponent,
    MenuComponent,
    LogoutComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass : HttpInterceptorService, multi: true},
    {provide: LocationStrategy, useClass : HashLocationStrategy}
 ],
  bootstrap: [AppComponent]
})
export class AppModule { }

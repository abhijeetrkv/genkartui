import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { User } from '../user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  username : string | undefined
  userEmail : string | undefined
  loggedInUser : User | undefined

  ngOnInit(): void {
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');

    this.username = this.loggedInUser?.firstName+" "+this.loggedInUser?.lastName;
    this.userEmail = this.loggedInUser?.email; 
  }

}

import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../service/file-upload.service';
import * as fileSaver from 'file-saver';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TemplateService } from '../service/data/template.service';
import { LoginService } from '../service/login.service';
import { User } from '../user';
import { LoaderService } from '../service/loader/loader.service';
import { API_URL } from '../app.constants';
import { Subscription } from 'rxjs/internal/Subscription';
import { interval } from 'rxjs';


export class Template {
  constructor(
    public id: number,
    public templateName: string,
    public userId: number,
    public dateModifed: Date,
    public image_type: number
  ) {

  }
}

@Component({
  selector: 'app-generate',
  templateUrl: './generate.component.html',
  styleUrls: ['./generate.component.css']
})
export class GenerateComponent implements OnInit {

  file!: File; // Variable to store file  
  zipfile!: File; // Variable to store zip file
  logoImage!: File;
  signImage!: File;  
  templates: Template[] | undefined
  userId: number | undefined
  loggedInUser: User | undefined
  selectedValue = '0'
  showSpinner = false
  progressPercentage = 0
  sub: Subscription | undefined;
  selectedTemplateID: String | undefined;
  imageType: String | undefined;

  // Inject service  
  constructor(private fileUploadService: FileUploadService,
    private templateService: TemplateService,
    private loginService: LoginService,
    public loaderService: LoaderService) { }

  ngOnInit(): void {
    this.getUserAssignedTemplates();
  }

  // On file Select 
  onChange(event: any) {
    this.file = event.target.files[0];
  }

  // On Image zip Select 
  onChangeZip(event: any) {
    this.zipfile = event.target.files[0];
  }

  // On logo Image Select 
  onChangeLogoImage(event: any) {
    this.logoImage = event.target.files[0];
  }

  // On Sign Image Select 
  onChangeSignImage(event: any) {
    this.signImage = event.target.files[0];
  }

  // OnClick of button Generate Certificate 
  generateCertificate() {
    console.log(this.selectedValue)
    console.log(this.file);
    console.log(this.zipfile);
    console.log(this.logoImage);
    console.log(this.signImage);

    var error = false;

    if (this.file === undefined) {
      alert("Please select Candidate source file.")
      error = true;
    }

    if (this.imageType == '1' && this.zipfile === undefined) {
      alert("Please attach Candidate Image file zip.")
      error = true;
    }

    if ((this.imageType == '2' || this.imageType == '3') && this.logoImage === undefined) {
      alert("Please select Template logo Image.")
      error = true;
    }

    if (this.imageType == '3' && this.signImage === undefined) {
      alert("Please attach Template sign Image.")
      error = true;
    }

    if (!error) {
      this.selectedTemplateID = this.selectedValue.split("#", 2)[0];

      //console.log(this.selectedTemplateID)
      this.showSpinner = true;

      //in 0.5 seconds get progress
      this.sub = interval(500)
        .subscribe((val) => { this.getprogressBarPercentage(); });

      if (this.imageType == '1'){
        this.fileUploadService.generateCertificate(this.selectedTemplateID, this.file, this.zipfile, undefined, undefined).subscribe(
          data => {
            //console.log(data)
            if(data == 'Candidate Data not found, Please check Sheet name.'){
              alert(data);
              this.showSpinner = false
              this.sub?.unsubscribe();
            }else{
              console.log("Certificates generated successfully at path " + data);
              this.download()
              this.sub?.unsubscribe();
            }
          },
          error => {
            console.log(error)
            alert("Error occured while generating certificates. Please contact System Administrator.");
            this.sub?.unsubscribe();
          }
        );
      }else if (this.imageType == '2'){
        this.fileUploadService.generateCertificate(this.selectedTemplateID, this.file, undefined, this.logoImage, undefined).subscribe(
          data => {
            //console.log(data)
            if(data == 'Candidate Data not found, Please check Sheet name.'){
              alert(data);
              this.showSpinner = false
              this.sub?.unsubscribe();
            }else{
              console.log("Certificates generated successfully at path " + data);
              this.download()
              this.sub?.unsubscribe();
            }
          },
          error => {
            console.log(error)
            alert("Error occured while generating certificates. Please contact System Administrator.");
            this.sub?.unsubscribe();
          }
        );
      }else if (this.imageType == '3'){
        this.fileUploadService.generateCertificate(this.selectedTemplateID, this.file, undefined, this.logoImage, this.signImage).subscribe(
          data => {
            console.log(data)
            if(data == 'Candidate Data not found, Please check Sheet name.'){
              alert(data);
              this.showSpinner = false
              this.sub?.unsubscribe();
            }else{
              console.log("Certificates generated successfully at path " + data);
              this.download()
              this.sub?.unsubscribe();
            }
          },
          error => {
            console.log(error)
            alert("Error occured while generating certificates. Please contact System Administrator.");
            this.sub?.unsubscribe();
          }
        );
      }else{
        this.fileUploadService.generateCertificate(this.selectedTemplateID, this.file, undefined, undefined, undefined).subscribe(
          data => {
            //console.log(data)
            if(data == 'Candidate Data not found, Please check Sheet name.'){
              alert(data);
              this.showSpinner = false
              this.sub?.unsubscribe();
            }else{
              console.log("Certificates generated successfully at path " + data);
              this.download()
              this.sub?.unsubscribe();
            }
          },
          error => {
            console.log(error)
            alert("Error occured while generating certificates. Please contact System Administrator.");
            this.sub?.unsubscribe();
          }
        );
      }
    }
  }

  download() {
    window.location.href = API_URL + '/downloadCertificateZip';
    this.showSpinner = false
  }

  getUserAssignedTemplates() {
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');

    this.templateService.getUserAssignedTemplates(this.loggedInUser?.id).subscribe(
      response => {
        //console.log(response);
        this.templates = response;
      }
    )

  }

  getprogressBarPercentage() {
    this.fileUploadService.getProgressBarPercentage().subscribe(
      (response: number) => {
        this.progressPercentage = response
      }
    )
  }

  getImageEnabledTemplateStatus() {
    //console.log("---------------------------"+this.selectedValue.split("#", 2)[1])
    this.imageType = this.selectedValue.split("#", 2)[1];
    return this.imageType;
  }
}
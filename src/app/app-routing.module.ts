import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorComponent } from './error/error.component';
import { FpasswordComponent } from './fpassword/fpassword.component';
import { GenerateComponent } from './generate/generate.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { ReportComponent } from './report/report.component';
import { RouteGuardService } from './service/route-guard.service';

const routes: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  {path:'profile', component: ProfileComponent, canActivate:[RouteGuardService]},
  {path:'generate', component: GenerateComponent, canActivate:[RouteGuardService]},
  {path:'report', component:ReportComponent, canActivate:[RouteGuardService]},
  {path:'dashboard', component:DashboardComponent, canActivate:[RouteGuardService]},
  {path:'login', component:LoginComponent},
  {path:'logout', component:LogoutComponent, canActivate:[RouteGuardService]},
  {path:'register', component:RegisterComponent},
  {path:'fpassword', component:FpasswordComponent, canActivate:[RouteGuardService]},
  {path:'**', component:ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

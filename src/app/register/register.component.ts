import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { User } from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstName=''
  lastName=''
  userEmail=''
  password=''

  constructor(private router: Router,
    private loginService: LoginService) { }

  ngOnInit(): void {
  }

  handleRegister() {
    let user = new User(0, this.userEmail, this.userEmail, this.password,
      this.firstName, this.lastName, 'Admin', new Date);
    this.loginService.registerfromSpringBoot(user).subscribe(
      data => {
        console.log(data)
        this.router.navigate(['login'])
      },
      error => {
        console.log(error)
      }

    )
  }

}

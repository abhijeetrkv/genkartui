import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { User } from '../user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  loggedInUser:User | undefined
  loggedInUserName : String | undefined

  ngOnInit(): void {
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser') || '{}');

    //console.log(this.loggedInUser)
    this.loggedInUserName = this.loggedInUser?.firstName+" "+this.loggedInUser?.lastName
    this.reloadIfNecessary()
  }

  reloadIfNecessary() {
    var isLoadedBefore = localStorage.getItem("IsLoadedBefore");
    if(isLoadedBefore=="true")
       return;
    else {
    localStorage.setItem("IsLoadedBefore","true");
    window.location.reload()
    }
  }
}
